%bcond_without sysprof

Name:           libsoup
Version:        2.74.3
Release:        5
Summary:        An HTTP library implementation
License:        LGPL-2.0-only
URL:            https://wiki.gnome.org/Projects/libsoup
Source0:        https://download.gnome.org/sources/%{name}/2.74/%{name}-%{version}.tar.xz

Patch6000:      backport-skip-tls_interaction-test.patch
Patch6001:      backport-libxml-2.12.patch
Patch6002:      backport-CVE-2024-52530.patch
Patch6003:      backport-0001-CVE-2024-52531.patch
Patch6004:      backport-0002-CVE-2024-52531.patch
Patch6005:      backport-0001-CVE-2024-52532.patch
Patch6006:      backport-0002-CVE-2024-52532.patch

BuildRequires: meson >= 0.50
BuildRequires: pkgconfig(gio-2.0) >= 2.58
BuildRequires: pkgconfig(glib-2.0) >= 2.58
BuildRequires: pkgconfig(gmodule-2.0) >= 2.58
BuildRequires: pkgconfig(gobject-2.0) >= 2.58
BuildRequires: pkgconfig(libbrotlidec)
BuildRequires: pkgconfig(libpsl) >= 0.20
BuildRequires: pkgconfig(libxml-2.0)
BuildRequires: pkgconfig(sqlite3)
BuildRequires: pkgconfig(zlib)
BuildRequires: glib-networking
BuildRequires: gettext
BuildRequires: gtk-doc
BuildRequires: krb5-devel
BuildRequires: vala
BuildRequires: /usr/bin/g-ir-scanner
BuildRequires: /usr/bin/ntlm_auth
%if %{with sysprof}
BuildRequires: pkgconfig(sysprof-capture-4)
%endif

Requires:       glib2 glib-networking

%description
libsoup is an HTTP client/server library for GNOME. It uses GObjects and the glib main loop,
to integrate well with GNOME applications, and also has a synchronous API,
for use in threaded applications.

%package        devel
Summary:        Header files for %{name}
Requires:       %{name} = %{version}-%{release}

%description    devel
The %{name}-devel package contains libraries and header files for %{name}.

%package_help

%prep
%autosetup -n %{name}-%{version} -p1

%build
%meson -Dgtk_doc=true -Dsysprof=%{?with_sysprof:enabled}%{?!with_sysprof:disabled}
%meson_build

%install
%meson_install
sed -i 's/idm[0-9]\{5,32\}/idm12345678912345/g' %{buildroot}%{_datadir}/gtk-doc/html/libsoup-2.4/ix01.html

%check
%meson_test

%find_lang %{name}

%files -f %{name}.lang
%doc AUTHORS
%license COPYING
%{_libdir}/*.so.*
%{_libdir}/girepository-1.0/Soup*2.4.typelib

%files devel
%{_includedir}/%{name}*-2.4
%{_libdir}/*.so
%{_libdir}/pkgconfig/*.pc
%{_datadir}/gir-1.0/Soup*2.4.gir
%{_datadir}/vala/vapi/libsoup-2.4.*

%files help
%doc README NEWS
%{_datadir}/gtk-doc/html/libsoup-2.4

%changelog
* Thu Nov 14 2024 xinghe <xinghe2@h-partners.com> - 2.74.3-5
- Type:cves
- ID:CVE-2024-52530 CVE-2024-52531 CVE-2024-52532
- SUG:NA
- DESC:fix CVE-2024-52530 CVE-2024-52531 CVE-2024-52532

* Wed Oct 30 2024 Funda Wang <fundawang@yeah.net> - 2.74.3-4
- fix build with libxml 2.12
- conditioned build sysprof support

* Thu Apr 18 2024 zhangpan <zhangpan103@h-partners.com> - 2.74.3-3
- Rebuild for next release

* Mon Feb 19 2024 hanhuihui <hanhuihui5@huawei.com> - 2.74.3-2
- rebuild for glib2 without sysprof

* Mon Jan 2 2023 lin zhang <lin.zhang@turbolinux.com.cn> - 2.74.3-1
- update to 2.74.3

* Wed Oct 26 2022 zhouwenpei <zhouwenpei1@h-partners.com> - 2.74.2-2
- Rebuild for next release

* Thu Dec 09 2021 liuyumeng <liuyumeng5@huawei.com> - 2.74.2-1
- update to libsoup-2.74.2

* Mon Apr 19 2021 zhanzhimin<zhanzhimin@huawei.com> - 2.72.0-3
- DESC:fix the complie failure due to glib-networking upgrade

* Mon Apr 19 2021 zhanzhimin<zhanzhimin@huawei.com> - 2.72.0-2
- Type:bugfix
- CVE:NA
- SUG:NA
- DESC:sed html idm for eliminate difference

* Wed Jan 27 2021 hanhui <hanhui15@huawei.com> - 2.72.0-1
- Type: enhancement
- ID:   NA
- SUG:  NA
- DESC: update to 2.72.0

* Thu Jul 23 2020 openEuler Buildteam <buildteam@openeuler.org> - 2.71.0-1
- Type:enhancement
- ID:NA
- SUG:NA
- DESC:upgrade to 2.71.0

* Wed Aug 28 2019 openEuler Buildteam <buildteam@openeuler.org> - 2.66.1-1
- Package init
